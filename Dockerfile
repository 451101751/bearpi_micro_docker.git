FROM ubuntu:20.04
RUN echo "root:root" | chpasswd
WORKDIR /root
RUN mkdir ~/tools
COPY mkimage.stm32 ./tools/
RUN chmod 777 ~/tools/mkimage.stm32
COPY build.sh ./
COPY requirements.txt ./
COPY pip.conf /etc/pip.conf
RUN mkdir /code
COPY build_code_server.sh /code/
COPY build_micro_code.sh /code/
RUN ln -fs /bin/bash /bin/sh && chmod -x ~/build.sh
ENV TZ Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN ["/bin/bash", "-c", "source build.sh"]
WORKDIR /code
RUN ["/bin/bash", "-c", "source build_code_server.sh"]
ENV PASSWORD=code-server
ADD start.sh /
RUN chmod +x /start.sh && echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
CMD ["/start.sh"]
