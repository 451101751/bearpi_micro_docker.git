#!/bin/bash
cp -a /etc/apt/sources.list /etc/apt/sources.list.bak
sed -i "s@http://.*archive.ubuntu.com@http://repo.huaweicloud.com@g" /etc/apt/sources.list
sed -i "s@http://.*security.ubuntu.com@http://repo.huaweicloud.com@g" /etc/apt/sources.list

apt-get update
apt-get install -y build-essential gcc g++ make zlib* libffi-dev e2fsprogs pkg-config flex bison perl bc openssl libssl-dev libelf-dev libc6-dev binutils binutils-dev libdwarf-dev u-boot-tools mtd-utils gcc-arm-linux-gnueabi cpio device-tree-compiler net-tools openssh-server git vim openjdk-11-jre-headless dosfstools mtools curl
apt-get -y install python3.8
apt-get -y install python3-pip
rm /usr/bin/python
ln -s python3.8 /usr/bin/python
pip3 install -r requirements.txt

cat >>~/.bashrc<<EOF
export PATH=~/.local/bin:$PATH
export PATH=~/tools:$PATH
EOF

source ~/.bashrc


