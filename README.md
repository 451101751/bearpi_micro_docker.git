# bearpi_micro
鸿蒙 iot 小熊派-Micro ubuntu20.04 开发环境
## 使用方式一、直接获取镜像
```shell
docker run -it --name bearpi_micro -v ~/code:/code thne/bearpi_micro:v1
```
## 使用方式二、自己构建
### 1. 拉取本代码仓
git clone https://gitee.com/451101751/bearpi_micro_docker.git
### 2. 构建镜像
```shell
docker build -t bearpi_micro -f Dockerfile . --no-cache                                       
```
### 3. 启动镜像
``` shell 
docker run -it --name bearpi_micro -p 8080:8080 -p 2222:22 bearpi_micro # 8080 为code_server 默认端口 22 为 ssh,sftp默认端口

docker run -d --name bearpi_micro -p 8080:8080 -p 2222:22 bearpi_micro # -d 后台运行
```

### 4. 获取源码和编译方式请参考官方教程
### 5. 参考 /code下面的两个脚本 构建micro或者搭建code-server
