#!/bin/bash
git clone https://gitee.com/bearpi/bearpi-hm_micro_small.git
cd bearpi-hm_micro_small
echo "hb set -root . -p"

echo "hb build -t notest --tee -f"

echo "cp out/bearpi_hm_micro/bearpi_hm_micro/OHOS_Image.stm32 applications/BearPi/BearPi-HM_Micro/tools/download_img/kernel/"
echo "cp out/bearpi_hm_micro/bearpi_hm_micro/rootfs_vfat.img applications/BearPi/BearPi-HM_Micro/tools/download_img/kernel/"
echo "cp out/bearpi_hm_micro/bearpi_hm_micro/userfs_vfat.img applications/BearPi/BearPi-HM_Micro/tools/download_img/kernel/"
